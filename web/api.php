<?php
/* Include Models */
include '../Model/Map.php';
include '../Model/MarsRover.php';


/* Include Controller */
include '../Controller/MappingController.php';
include '../Controller/MarsRoverController.php';
include '../Controller/ApiActionsController.php';


/* LoadConfig */
$fileconf = '../conf/conf.json';
$confJson = file_get_contents($fileconf);
$confArr = json_decode($confJson,true);

/*Load temp map file*/
$file_tmp_map = '../temp/mapgenerated.json';

/* Load TMP Rover */
$file_tmp_rover = '../temp/rovergenerated.json';

/* Create Map Object */
$Map = new Map();

/* instantiate the controllers */
$Mapping_controller = new MappingController();
$Mars_rover_controller = new MarsRoverController();
$api_actions_controller = new ApiActionsController();

/* Get the action to do */
$action = '';
if(isset($_GET['action'])) {
    $action = $_GET['action'];
}



$return = [];

switch($action) {
    case 'start':
        /* Generates a new Rover Instance and a new Map if it's not generated
        It tries to read the parameters from get, if it can't it gets the default from the config file */
        if (isset($_GET['x'])) {
            $initialX = $_GET['x'];
        } else {
            $initialX = $confArr['defaultPosition']['x'];
        }
        if (isset($_GET['y'])) {
            $initialY = $_GET['y'];
        } else {
            $initialY = $confArr['defaultPosition']['y'];
        }
        if (isset($_GET['o'])) {
            $orientation = $_GET['o'];
        } else {
            $orientation = $confArr['defaultOrientation'];
        }

        $msg = $api_actions_controller->StartAction($confArr, $Mapping_controller, $file_tmp_map, $Map, $Mars_rover_controller, $file_tmp_rover, $return, $initialX, $initialY, $orientation);
        break;

    case 'move':
        /* move the rover */
        if(!isset($Mars_rover)) {
            $Mars_rover = $Mars_rover_controller->initializeMarsRover($file_tmp_rover);
        }
        if($Mars_rover != false) {
            $moveinstructions = $_GET['move'];
            $instructionsArray = str_split($moveinstructions);

            $orientation = strtoupper($Mars_rover->getOrientation());

            $Mapping_controller->InitializeMap($file_tmp_map,  $Map, $confArr);
            $msg = $Mars_rover_controller->moveRoverAction($instructionsArray, $orientation, $Mars_rover, $file_tmp_rover, $Mapping_controller, $Map, $confArr);
        } else {
            $msg = $confArr["ErrorMessages"]["Unreachable"];
        }
        break;

    default:
        /* default action for not set actions. Also there's a chance to have a crytical error */
        $rand = rand(0,100);
        if($rand == $confArr['reset']) {
            $Mars_rover_controller->restart($file_tmp_rover, $file_tmp_map);
            $msg =  $confArr["ErrorMessages"]["Critical"];
        } else {
            $msg = $confArr["ErrorMessages"]["Command"];
        }
}




$return['msg'] = $msg;

die(json_encode($return));
