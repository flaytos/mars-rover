<?php


class MarsRover
{
    private $id;
    private $position = [
        'x' => '',
        'y' => ''
    ];
    private $orientation;
    private $message;
    private $error;

    public function __construct() {
        $this->id = rand(100000,999999);
        $error = false;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPositionX() {
        return $this->position['x'];
    }

    /**
     * @return int
     */
    public function getPositionY() {
        return $this->position['y'];
    }

    /**
     * @return string
     */
    public function getOrientation() {
        return $this->orientation;
    }

    /**
     * @param $positionX
     */
    public function setPositionX($positionX) {
        $this->position['x'] = $positionX;
    }

    /**
     * @param $positionY
     */
    public function setPositionY($positionY) {
        $this->position['y'] = $positionY;
    }

    /**
     * @param $orientation
     */
    public function setOrientation($orientation) {
        $this->orientation = $orientation;
    }


    public function setMessage($message) {
        $this->message = $message;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
         $this->error = $error;
    }

}