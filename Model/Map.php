<?php


class Map
{
    private $map = [];

    public function __construct() {

    }

    /**
     * @return array
     */
    public function getMap() {
        return $this->map;
    }

    /**
     * @param $map
     */
    public function setMap($map) {
        $this->map = $map;
    }
}