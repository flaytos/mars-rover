# Technical test - Mars rover

## Usage
### Start the Rover
_The Mars rover needs to be started before moving._
#### Example
web/api.php?action=start&x=3&y=6&o=N

Parameters:
* Action: It defines the action to do, Must be start
* X: X position where the rover will spawn. Using the default config, this value can be between 1 and 200. If it's not there it will take the default value defined in the config file with the parameter defaultPosition [x]
* y: Y position where the rover will spawn. Using the default config, this value can be between 1 and 200. If it's not there it will take the default value defined in the config file with the parameter defaultPosition [y]
* o: The orientation of the rover at the spawn. The correct values are: [N, E, S, W]. If it's not set, it will take the default value at the parameter defaultOrientation
### Move the Rover
#### Example
web/api.php?action=move&move=RRFFL
Parameters:
* Action: It defines the action to do, Must be move
* Move: It defines the set of movements the rover will do. It can have the next values: F - will make the Rover move forward. L - will make the rover turn left and move forward. R -  will make the rover turn right and move forward
You can send more than one move in the "move" parameter, for example RRFFL