<?php


class MarsRoverController
{
    /**
     * It Removes the files with the generated map and rover
     */
    public function restart($file_tmp_rover, $file_tmp_map)
    {
        if(file_exists($file_tmp_rover)) unlink($file_tmp_rover);
        if(file_exists($file_tmp_map)) unlink($file_tmp_map);
    }

    /**
     * It generates the mars rover and tries to spawn at the specified location
     */
    public function MarsRoverStart($file_tmp_rover,$map,$config,$positionX,$positionY,$orientation)
    {
        $message = '';
        /**
         * Check if the Rover is already there, if it is, it loads it
         */
        if (!file_exists($file_tmp_rover)) {
            $Mars_rover = new MarsRover();
            $this->saveMarsRover($Mars_rover, $file_tmp_rover);
        } else {
            $Mars_rover = $this->initializeMarsRover($file_tmp_rover);
        }

        /**
         * Check if the Rover is "generated" but without being spawned
         */
        if( $Mars_rover != false and $Mars_rover->getPositionX() =='' and  $Mars_rover->getPositionY() == '') {
            /**
             * Checks the spawn viability
             */
            if(isset($map[$positionY][$positionX])) {
                $map_data = $map[$positionY][$positionX];
                if(!in_array($map_data, $config['landtype_good'])) {
                    $Mars_rover->setError(true);
                    $message = $config['ErrorMessages']['Terrain'];
                } else {
                    if(!in_array($orientation, $config['correctOrientation'])) {
                        $Mars_rover->setError(true);
                        $message = $config['ErrorMessages']['Orientation'];
                    } else {
                        $Mars_rover->setPositionX($positionX);
                        $Mars_rover->setPositionY($positionY);
                        $Mars_rover->setOrientation($orientation);
                        $message = $config['OkResponses']['Initial'];
                    }
                }
            } else {
                /**
                 * Error, The Mars Rover is already spawned and has a location
                 */
                $Mars_rover->setError(true);
                $message = str_replace([$config['messageReplaces']['x'],$config['messageReplaces']['y']],[$positionX, $positionY],$config["ErrorMessages"]["Location"]);
            }
        } else {
            /**
             * Error, The Mars Rover can't spawn at the specified location
             */
            $Mars_rover->setError(true);
            $message = str_replace([$config['messageReplaces']['x'],$config['messageReplaces']['y']],[$positionX, $positionY],$config["ErrorMessages"]["Spawn"]);
        }
        $Mars_rover->setMessage($message);
        return $Mars_rover;
    }

    /**
     * it saves the mars rover into a file to be able to load it later
     */
    public function saveMarsRover(MarsRover $Mars_rover, $file_tmp_rover)
    {
        $serialized_rover = serialize($Mars_rover);
        file_put_contents($file_tmp_rover, $serialized_rover);
    }

    /**
     * Reads the actual rover spawned
     */
    public function initializeMarsRover($file_tmp_rover)
    {
        if (file_exists($file_tmp_rover)) {
            $serialized_rover = file_get_contents($file_tmp_rover);
            $Mars_rover = unserialize($serialized_rover);
            return $Mars_rover;
        }
        return false;

    }

    /**
     * Method to move forward the Rover
     */
    public function moveForward($Mapping_controller, Map $Map, $confArr, $Mars_rover,  $messagesArray, $file_tmp_rover)
    {
        $orientation = $Mars_rover->getOrientation();
        $moveError = false;
        $x = $Mars_rover-> getPositionX();
        $y = $Mars_rover-> getPositionY();
        /**
         * It gets the future location taking into consideration the orientation of the rover
         */
        switch($orientation){
            case 'N':
            case 'n':
                $futureY = $y + 1;
                $futureX = $x;
            break;

            case 'W':
            case 'w':
                $futureY = $y;
                $futureX = $x - 1;
                break;

            case 'E':
            case 'e':
                $futureY = $y;
                $futureX = $x + 1;
                break;

            case 'S':
            case 's':
                $futureY = $y - 1;
                $futureX = $x;
                break;

        }
        /**
         * it checks if the move action is possible
         */
        if ($Mapping_controller->checkMovePossible($Map, $futureX, $futureY, $confArr)) {
            $Mars_rover->setPositionY($futureY);
            $Mars_rover->setPositionX($futureX);
            $Mars_rover->setError = false;
            //str_replace([])
            $messagesArray [] = str_replace([$confArr['messageReplaces']['x'],$confArr['messageReplaces']['y'],$confArr['messageReplaces']['o']],[$futureX, $futureY, $Mars_rover->getOrientation()],$confArr["OkResponses"]["CorrectMove"]);
            $this->saveMarsRover($Mars_rover, $file_tmp_rover);
        } else {
            $reason = $Mapping_controller->getLandType($Map,$futureX,$futureY,$confArr);
            $messagesArray [] = str_replace([$confArr['messageReplaces']['x'],$confArr['messageReplaces']['y'],$confArr['messageReplaces']['l']],[$futureX, $futureY,$reason],$confArr["ErrorMessages"]["Notmove"]);
            $messagesArray [] = str_replace([$confArr['messageReplaces']['x'],$confArr['messageReplaces']['y'],$confArr['messageReplaces']['o']],[$Mars_rover->getPositionX(),$Mars_rover->getPositionY(), $Mars_rover->getOrientation()],$confArr["ErrorMessages"]["Standing"]);
            $moveError = true;
        }
        return array($Mars_rover, $messagesArray, $moveError);

    }

    /**
     * Method to turn the rover to the correct orientation and move 1 position forward
     */
    function moveRoverAction(array $instructionsArray, $orientation, $Mars_rover, $file_tmp_rover, $Mapping_controller, $Map, $confArr)
    {
        $messagesArray = [];
        $moveError = false;
        foreach ($instructionsArray as $inst) {
            if(in_array(strtoupper($inst),$confArr['correctInstruction'])){
                if ($moveError == false) {
                    if (strtolower($inst) == "l" || strtolower($inst) == "r") {
                        /**
                         * Changing to the correct orientation if it's required
                         */
                        switch ($orientation) {
                            case "N":
                            case "n":
                                if (strtoupper($inst) == "L") {
                                    $Mars_rover->setOrientation('W');
                                } else {
                                    $Mars_rover->setOrientation('E');
                                }
                                break;
                            case "W":
                            case "w":
                                if (strtoupper($inst) == "L") {
                                    $Mars_rover->setOrientation('S');
                                } else {
                                    $Mars_rover->setOrientation('N');
                                }
                                break;

                            case "S":
                            case "s":
                                if (strtoupper($inst) == "L") {
                                    $Mars_rover->setOrientation('E');
                                } else {
                                    $Mars_rover->setOrientation('W');
                                }
                                break;

                            case "E":
                            case "e":
                                if (strtoupper($inst) == "L") {
                                    $Mars_rover->setOrientation('N');
                                } else {
                                    $Mars_rover->setOrientation('S');
                                }
                                break;

                            default:
                                $moveError = true;
                                break;
                        }
                        $this->saveMarsRover($Mars_rover, $file_tmp_rover);
                    }
                    /** if the Rover has a valid orientation, it moves forward */
                    if ($moveError == false) {
                        list($Mars_rover, $messagesArray, $moveError) = $this->moveForward($Mapping_controller, $Map, $confArr, $Mars_rover, $messagesArray, $file_tmp_rover);
                    } else {
                        $messagesArray[] = $confArr["ErrorMessages"]["Imporien"];
                    }
                }
            } else {
                if($moveError == false) {
                    $moveError = true;
                    $messagesArray[] = str_replace([$confArr['messageReplaces']['i']],[$inst],$confArr["ErrorMessages"]["Nomovecommand"]);
                }
            }

        }
        return $messagesArray;
    }
}