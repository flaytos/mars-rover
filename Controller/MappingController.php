<?php


class MappingController
{
    /**
     * Method to initialize the map where the Rover will land
     */
    function InitializeMap($file_tmp_map,  &$Map, $confArr)
    {
        /* Check existing map */
        if (!file_exists($file_tmp_map)) {
            $this->generateMap($Map, $confArr['MapDim']['x'], $confArr['MapDim']['y'], $confArr['minlandtypes'], $confArr['maxlandtypes']);
            $jsonMap = json_encode($Map->getMap());
            file_put_contents($file_tmp_map, $jsonMap);
        } else {
            $jsonMap = file_get_contents($file_tmp_map);
            $mapArr = json_decode($jsonMap, true);
            $Map->setMap($mapArr);
        }
    }

    /**
     * Method to generate the map
     */
    private function generateMap(&$Map,$maxX,$maxY,$minlandtypes, $maxlandtypes) {
        $mapping = [];
        for($y = 1;$y<=$maxY;$y++) {
            for($x = 1;$x<=$maxX;$x++) {
                $mapping[$x][$y] = rand($minlandtypes,$maxlandtypes);
            }
        }
        $Map->setMap($mapping);
    }

    /**
     * Checks if it's possible to move the Rover to the specified location
     */
    public function checkMovePossible($Map,$x,$y,$confArr) {
        $mapArr = $Map->getMap();
        if(isset($mapArr[$y][$x])) {
            $mapdata = $mapArr[$y][$x];
            if(in_array($mapdata, $confArr['landtype_good'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * return what kind of terrain is at the specified locations
     */
    public function getLandType($Map,$x,$y,$confArr) {
        $mapArr = $Map->getMap();
        if(isset($mapArr[$y][$x])) {
            $mapdata = $mapArr[$y][$x];
            $return = $confArr['landtypes'][$mapdata];
        } else {
            $return = $confArr['ErrorMessages']['noMap'];
        }

        return $return;
    }
}