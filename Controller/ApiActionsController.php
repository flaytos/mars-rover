<?php


class ApiActionsController
{
    /**
     * Method to generate the map if it's not generated and initialize the rover
     */
    public function StartAction($confArr, $Mapping_controller, $file_tmp_map,  $Map,  $Mars_rover_controller, $file_tmp_rover, $return,$initialX,$initialY,$orientation)
    {
        $Mapping_controller->InitializeMap($file_tmp_map, $Map, $confArr);

        $Mars_rover = $Mars_rover_controller->MarsRoverStart($file_tmp_rover, $Map->getMap(), $confArr, $initialX, $initialY, $orientation);
        $return['rover_id'] = $Mars_rover->getId();
        if ($Mars_rover->getError() == true) {
            $msg = $Mars_rover->getMessage();
        } else {
            $msg = $confArr['OkResponses']['Initial'];
        }

        $Mars_rover_controller->saveMarsRover($Mars_rover, $file_tmp_rover);
        //return array($Map, $return, $msg, $Mars_rover);
        return $msg;
    }
}